module Streamly.External.Serialize where

import qualified Data.Serialize
import qualified Data.Binary as Binary
import qualified Flat
import qualified Streamly
import qualified Streamly.Prelude as Streamly


newtype WrappedStream t m a = WrappedStream { unwrapStream :: t m a }

instance (Serialize.Serialize a) => Serialize.Serialize (WrappedStream Streamly.SerialT Identity a) where
  put ws =
    let length = Streamly.length $ unwrapStream s
    in put length >> (mapM_ put $ runIdentity $ Streamly.toList s)
